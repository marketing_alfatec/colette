<?php

abstract class Module extends ModuleCore
{
	protected function getCacheId($name = null)
    {
        if (version_compare(_PS_VERSION_,'1.5.3.1','>'))
            return parent::getCacheId($name);
        else
            return false;
    }
}

