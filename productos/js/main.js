$(document).ready(function(){
	$.fn.ajaxSwitcher({
		articleTopPosition:255,
		menuInit:function (mainMenu, classMenu, classSubMenu){
			$(">a", mainMenu).each(function(){
				var textThis = $(this).text()
				$(this).html("<div class='over'></div><div class='itemText'>"+textThis+"</div>")
			})
			$(">li>a", classSubMenu).each(function(){
				var textThis = $(this).text()
				$(this).html("<div class='itemText'>"+textThis+"</div>")
			})
		},
		buttonOver:function(item){
			$(">a>.over", item).stop(true).animate({scale:1}, 300, "easeOutCubic");
		},
		buttonOut:function(item){
			$(">a>.over", item).stop(true).animate({scale:0}, 300, "easeOutCubic");
		},
		subMenuShow:function(item, sClass){
			$(">"+sClass, item).stop(true, true).slideDown(600, "easeOutCubic");
		},
		subMenuHide:function(item, sClass){
			$(">"+sClass, item).stop(true, true).delay(300).slideUp(600, "easeOutCubic");
		},
		subMenuButtonOver:function(item){
			$(">a>.itemText", item).stop(true).animate({color:"#692312"}, 600, "easeOutCubic");	
		},
		subMenuButtonOut:function(item){
			$(">a>.itemText", item).stop(true).animate({color:"#fff"}, 600, "easeOutCubic");	
		},
		pageInit:function (pageList){
			$("article", pageList).css({left:$(document).width()})
		},	
		currPageAnimate:function(page){
			page.css({left:$(document).width(), "display":"block"}).stop(true).animate({left:0}, 900, "easeOutCubic")
			$("#galleryDiscription, #previewHolder").stop(true).animate({top:"100%"}, 700, "easeInOutCubic", function(){
				$("#galleryDiscription, #previewHolder").css({"display":"none"})	
			});
		},
		prevPageAnimate:function(page){
			page.stop(true).animate({left:-$(document).width()}, 900, "easeInCubic", function(){
				page.css({"display":"none"}).find(".box").contents().remove();
			});
		},
		backToSplash:function(){
			$("#galleryDiscription, #previewHolder").css({"display":"block"}).stop(true).delay(500).animate({top:"50%"}, 700, "easeInOutCubic");
		},
		pageLoadComplete:function(){
			$("a[rel^='fancybox']").fancybox({'speedIn'  : 300, 'speedOut'  : 300});
			$(".zoom-icon").stop(true).fadeTo(0, 0);
			$(".thumbnail").live("mouseover",
		 		function(){
					$(".zoom-icon", this).stop(true).fadeTo(300, 0.75);
				}
			)
			$(".thumbnail").live("mouseout",
				function(){
					$(".zoom-icon", this).stop(true).fadeTo(300, 0);
				}
			)
		}
	})
	$(window).load(function(){
		
		// Init for audiojs
		audiojs.events.ready(function() {
			var as = audiojs.createAll();
		});
		/*!!!!HACK!!!!!*/
		$("a:[href='#']").live("click", function(){return false})
		/*!!!!end HACK!!!!!*/
		
		$("#backToTop").hover(
			function(){
				$(this).animate({borderColor:"#fff"}, 300, "easeInOutCubic");
			}, function(){
				$(this).animate({borderColor:"#C55333"}, 300, "easeInOutCubic");
			}
		)
		
		$("#galleryHolder").gallerySplash();
		$(".social-networks>li>a>img").hover(
			function(){
				$(this).stop(true).animate({top:-4}, 300, "easeInOutCubic");
			}, function(){
				$(this).stop(true).animate({top:0}, 300, "easeInOutCubic");
			}
		)		
		$("#spinnerBG").delay(0).animate({scale:0}, 800, "easeInOutCubic", function(){$("#spinnerBG").remove()});		
   	});
})