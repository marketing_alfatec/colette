﻿/*
*Version: 1.1.11 - last update: 04.07.2012;
*Author: Behaart;
*/
(function($){
	$.fn.ajaxSwitcher=function(o){
		var sO= {
			//mainPageHash:"#!/splash-page",
			//FGM 
			mainPageHash:"",
			pageList:"#pageList",
			mainMenu:"#mainMenu>li",
			classMenu:".menu",
			classSubMenu:".sub-menu",
			siteWrapper:"#wrapper",
			pageSpinner:"#pageSpinner",
			noChangeLinks:".comment-edit-link, .thumbnail, .comment-reply-link, .thumbnailGallery, .file, a:[href*='profile.php'], a:[href*='wp-login.php'][class!='logOutPage'], a:[href*='wp-admin'], a:[href*='feed']",
			articleTopPosition:100,
			scrollToTop:true,
			
			menuInit:function(mainMenu, classMenu, classSubMenu){},
			buttonOver:function(item){},
			buttonOut:function(item){},
			subMenuButtonOver:function(item){},
			subMenuButtonOut:function(item){},
			subMenuShow:function(item){},
			subMenuHide:function(item){},
			pageInit:function(pageList){},
			currPageAnimate:function(){},
			prevPageAnimate:function(){},
			backToSplash:function(){},
			pageLoadComplete:function(){}
		},
		lpC=true,
		currPageHash,
		currPageHolder = "#pageHolder_1",
		prevPageHash,
		prevPageHolder = "#pageHolder_2",
		MSIE9 = ($.browser.msie) && ($.browser.version == 9),
		siteURL = $("#logo>a").attr("href"),
		sitePath= $("#logo>a").attr("class");
		$.extend(sO, o);
		
		function init(){
			if(MSIE9){
				$("html").css({"overflow-y":"scroll"});
			}else{
				$("body").css({"overflow-y":"scroll"});
			}
			$(window).bind("hashchange", hashC);
			$(window).scroll(scrollPageToTop);
			$(window).bind("mousewhell", function(){$("body, html").stop(true)});
			$("#backToTop").bind("click", clickBackToTop)//.fadeOut(400);
			$("a").live("click", noLoadPage);
			sO.menuInit(sO.mainMenu, sO.classMenu, sO.classSubMenu);
			sO.pageInit(sO.pageList);
			changeLinks(sO.siteWrapper);
			scrollPageToTop()			
			
			$("#logo>a").attr("href", sO.mainPageHash).removeClass();
			$(sO.classSubMenu).css({display:"none"})
			$.fn.ajaxForms();

			$(sO.mainMenu).hover(
				function(){
					if($(">a", this).attr("href") != currPageHash){
						$(this).addClass("itemHover");
						sO.buttonOver($(this));
					}	
					sO.subMenuShow($(this), sO.classSubMenu);
				},
				function (){
					if($(">a", this).attr("href") != currPageHash){
						$(this).removeClass("itemHover");
						sO.buttonOut($(this));
					}
					sO.subMenuHide($(this), sO.classSubMenu);
				}
			).trigger("mouseout");	
			$(">li", sO.classSubMenu).hover(
				function(){
					if($(">a", this).attr("href") != currPageHash){
						$(this).addClass("itemHover");
						sO.subMenuButtonOver($(this));
					}
					sO.subMenuShow($(this), sO.classSubMenu);
				},
				function(){
					if($(">a", this).attr("href") != currPageHash){
						$(this).removeClass("itemHover");
						sO.subMenuButtonOut($(this));
					}
					sO.subMenuHide($(this), sO.classSubMenu);
				}
			).trigger("mouseout");
			hashC();
		};
		function changeLinks(item){
			$("a:[href^='"+siteURL+"']", item).not(sO.noChangeLinks).each(function(){
				var linkUrl = $(this).attr("href");
				linkUrl = "#!/"+linkUrl.slice(siteURL.length, linkUrl.length);
				if(linkUrl.length==linkUrl.lastIndexOf("/")+1){
					linkUrl= linkUrl.slice(0, -1)
				}
				$(this).attr("href", linkUrl);
			})
		};
		function noLoadPage(){
			if(lpC==false && window.location.hash!=sO.mainPageHash){
				return false;
			}
		}
		function hashC(){
			var hashState = window.location.hash;
			lpC=false;
			
			if(currPageHolder == "#pageHolder_2"){
				prevPageHolder = "#pageHolder_2";
				currPageHolder = "#pageHolder_1";
			}else{
				prevPageHolder = "#pageHolder_1";
				currPageHolder = "#pageHolder_2";
			}
			
			if(window.location.href.lastIndexOf(".php")==-1){
				if(hashState.length < 4){
					window.location.hash=sO.mainPageHash;
				}
			}
			prevPageHash = currPageHash;
			currPageHash = window.location.hash.split("?")[0].split("page/")[0];
			if(currPageHash.length==currPageHash.lastIndexOf("/")+1){
				currPageHash= currPageHash.slice(0, -1)
			}	
			if(prevPageHash != currPageHash){
				sO.buttonOut($(">a:[href='"+prevPageHash+"']", sO.mainMenu).parent());
				$(">a:[href='"+prevPageHash+"']", sO.mainMenu).parent().removeClass("itemHover");
				sO.subMenuButtonOut($("a:[href='"+prevPageHash+"']", sO.classSubMenu).parent());
				$("a:[href='"+prevPageHash+"']", sO.classSubMenu).parent().removeClass("itemHover");
				
				if(!$(">a:[href='"+currPageHash+"']", sO.mainMenu).parent().hasClass("itemHover")){
					sO.buttonOver($(">a:[href='"+currPageHash+"']", sO.mainMenu).parent());
					$(">a:[href='"+currPageHash+"']", sO.mainMenu).parent().addClass("itemHover");
				}
				if(!$(">a:[href='"+currPageHash+"']", sO.classSubMenu).parent().hasClass("itemHover")){
					sO.subMenuButtonOver($("a:[href='"+currPageHash+"']", sO.classSubMenu).parent());
					$("a:[href='"+currPageHash+"']", sO.classSubMenu).parent().addClass("itemHover");
				}
			}
			if($(prevPageHolder).height()==0){
				$(prevPageHolder).css({display:"none"})	
			}				
			sO.prevPageAnimate($(prevPageHolder));
				
			if(window.location.hash!=sO.mainPageHash){
				ajaxLP();
			}else{
				backTS()
			}
		};
		function ajaxLP(Eror404){
			var pageURL;
				
			if(Eror404){
				pageURL =sitePath+"/404.php"
			}else{
				pageURL =siteURL+"/"+window.location.hash.substring(3, window.location.hash.length)
			}
			$(sO.pageSpinner).delay(400).fadeIn(300, "swing");
			
			$.ajax({
				url:pageURL,
				type:"GET",
				data:"ajaxRequest=true",
				cache: false,
	            success:function(data){ajaxLPС(data)},
	            error:function(data){ajaxLPE(data)}
			})	
		};
		function ajaxLPС(data){
			$(sO.pageSpinner).delay(400).fadeOut(300, "swing");
			$(currPageHolder).find(".box").html(data);
			
			sO.pageLoadComplete();	
			changeLinks($(currPageHolder));
			sO.currPageAnimate($(currPageHolder));
			lpC=true;
			setTimeout(sP, 200);
		};
		function ajaxLPE(data){
			ajaxLP(true)
		};
		function backTS(){
			sO.backToSplash()
			$(sO.pageSpinner).delay(400).fadeOut(300, "swing");
			setTimeout(sP, 200);
		};
		function sP(){
			var contentHeight= $(currPageHolder).outerHeight(true)+sO.articleTopPosition+$("#footer").outerHeight(true),
				winHash = window.location.hash,
				scrollPosition = 0,
				scrollID,
				strLenght = winHash.length;
			
			if(contentHeight<parseInt($("body").css("min-height")) || contentHeight<$(window).height()){
				contentHeight = "100%";
			}
			
			$("#wrapper").stop(true).animate({height:contentHeight}, 2000, "easeOutCubic");
			
			if(winHash.lastIndexOf("/#")!=-1){
				if(winHash.indexOf("?")!=-1){
					strLenght = winHash.indexOf("?")
				}
				scrollID=winHash.slice(winHash.lastIndexOf("/#")+1, strLenght);
				scrollPosition = $(scrollID).offset().top;
			}
			if(sO.scrollToTop){
				$("body, html").delay(450).stop(true).animate({scrollTop:scrollPosition}, 1000, "easeInOutCubic");	
			}
		};
		function scrollPageToTop(){
			if($("body").scrollTop()>0 || $("html").scrollTop()>0){
				$("#backToTop").stop().animate({bottom:45}, 800, "easeOutCubic");
			}else{
				$("#backToTop").stop(true, true).animate({bottom:-60}, 800, "easeOutCubic");
			}
		}
		function clickBackToTop(){
			$("body, html").stop(true, true).animate({scrollTop:0}, 800, "easeInOutCubic");
		} 
		init();
	}
})(jQuery)