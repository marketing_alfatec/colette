<?php
/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class StAddThisButton extends Module
{
    private $_html = '';
    public $fields_form;
    public $fields_value;
	public function __construct()
	{
		$this->name          = 'staddthisbutton';
		$this->tab           = 'front_office_features';
		$this->version       = '1.0';
		$this->author        = 'SUNNYTOO.COM';
		$this->need_instance = 0;

		parent::__construct();

		$this->displayName  = $this->l('Add this button');
		$this->description  = $this->l('The Largest Sharing and Social Data Platform.');
	}

	public function install()
	{
		if (!parent::install() 
            || !$this->registerHook('displayStBlogArticleFooter')
            || !$this->registerHook('displayProductSecondaryColumn')
            || !Configuration::updateValue('ST_ADDTHIS_STYLE', 2)
            || !Configuration::updateValue('ST_ADDTHIS_STYLE_FOR_BLOG', 0)
            || !Configuration::updateValue('ST_ADDTHIS_PUBID', '')
        )
			return false;
            
		$this->_clearCache('staddthisbutton.tpl');
		return true;
	}
	
	public function uninstall()
	{
		$this->_clearCache('staddthisbutton.tpl');   
		return parent::uninstall();
	}
    
    public function getContent()
	{
		if (isset($_POST['savestaddthisbutton']))
		{
            if (!Configuration::updateValue('ST_ADDTHIS_STYLE', (int)Tools::getValue('style'))
                || !Configuration::updateValue('ST_ADDTHIS_STYLE_FOR_BLOG', (int)Tools::getValue('style_for_blog'))
                || !Configuration::updateValue('ST_ADDTHIS_PUBID', (string)Tools::getValue('pubid'))
            )
                $this->_html .= count($errors) ? implode('',$error) : $this->displayError($this->l('Cannot update settings'));
            else
                $this->_html .= $this->displayConfirmation($this->l('Settings updated'));    
			
		    $this->_clearCache('staddthisbutton.tpl');        
        }
		$helper = $this->initForm();
        $helper->fields_value['style'] = (int)Configuration::get('ST_ADDTHIS_STYLE');
        $helper->fields_value['style_for_blog'] = (int)Configuration::get('ST_ADDTHIS_STYLE_FOR_BLOG');
        $helper->fields_value['pubid'] = Configuration::get('ST_ADDTHIS_PUBID');
		return $this->_html.$helper->generateForm($this->fields_form);
	}
    protected function initForm()
	{
		$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

		$this->fields_form[0]['form'] = array(
			'legend' => array(
				'title' => $this->l('Settings'),
			),
			'input' => array(
                array(
					'type' => 'radio',
					'label' => $this->l('Layouts:'),
					'name' => 'style',
					'class' => 't',
                    'default_value' => 0,
					'values' => array(
						array(
							'id' => 'style_one',
							'value' => 0,
							'label' => $this->l('Layouts 1')),
						array(
							'id' => 'style_two',
							'value' => 1,
							'label' => $this->l('Layouts 2')),
						array(
							'id' => 'style_three',
							'value' => 2,
							'label' => $this->l('Layouts 3')),
						array(
							'id' => 'style_four',
							'value' => 3,
							'label' => $this->l('Layouts 4')),
					),
                    'desc' => $this->l('Layouts 1').'<br/><img src="'.$this->_path.'views/img/style_1.jpg" /><br/>'.$this->l('Layouts 2').'<br/><img src="'.$this->_path.'views/img/style_2.jpg" /><br/>'.$this->l('Layouts 3').'<br/><img src="'.$this->_path.'views/img/style_3.jpg" /><br/>'.$this->l('Layouts 4').'<br/><img src="'.$this->_path.'views/img/style_4.jpg" /><br/>',
				), 
                array(
					'type' => 'radio',
					'label' => $this->l('Layouts for blog:'),
					'name' => 'style_for_blog',
					'class' => 't',
                    'default_value' => 0,
					'values' => array(
						array(
							'id' => 'style_one',
							'value' => 0,
							'label' => $this->l('Layouts 1')),
						array(
							'id' => 'style_two',
							'value' => 1,
							'label' => $this->l('Layouts 2')),
						array(
							'id' => 'style_three',
							'value' => 2,
							'label' => $this->l('Layouts 3')),
						array(
							'id' => 'style_four',
							'value' => 3,
							'label' => $this->l('Layouts 4')),
					),
				), 
                array(
					'type' => 'text',
					'label' => $this->l('Addthis ID:'),
					'name' => 'pubid',
					'size' => 64,
                    'desc' => $this->l('Input your own Addthis id ex.: ra-516bd3c977d5eb6c for analitycs.').'<br/><img src="'.$this->_path.'views/img/addthis_id.jpg" />',
				),
			),
			'submit' => array(
				'title' => $this->l('   Save   '),
				'class' => 'button'
			)
		);
		$helper = new HelperForm();
		$helper->module = $this;
		$helper->name_controller = 'staddthisbutton';
		$helper->identifier = $this->identifier;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		$helper->default_form_language = $default_lang;
		$helper->allow_employee_form_lang = $default_lang;
		$helper->toolbar_scroll = true;
		$helper->title = $this->displayName;
		$helper->submit_action = 'savestaddthisbutton';
        
        
        
		$helper->toolbar_btn =  array(
			'save' =>
			array(
				'desc' => $this->l('Save'),
				'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
			)
		);
		return $helper;
	}
    public function hookDisplayRightColumnProduct($params)
    {
		if (!$this->isCached('staddthisbutton.tpl', $this->stGetCacheId(1)))
    		$this->smarty->assign(array(
                'addthis_style' => Configuration::get('ST_ADDTHIS_STYLE'),
                'addthis_pubid' => Configuration::get('ST_ADDTHIS_PUBID'),
    		));
		return $this->display(__FILE__, 'staddthisbutton.tpl', $this->stGetCacheId(1));
    }
    public function hookDisplayLeftColumnProduct($params)
    {
        return $this->hookDisplayRightColumnProduct($params);
    }
    public function hookDisplayProductSecondaryColumn($params)
    {
        return $this->hookDisplayRightColumnProduct($params);
    }
    public function hookDisplayStBlogArticleFooter($params)
    {
		if (!$this->isCached('staddthisbutton.tpl', $this->stGetCacheId(2)))
    		$this->smarty->assign(array(
                'addthis_style' => Configuration::get('ST_ADDTHIS_STYLE_FOR_BLOG'),
                'addthis_pubid' => Configuration::get('ST_ADDTHIS_PUBID'),
    		));
		return $this->display(__FILE__, 'staddthisbutton.tpl', $this->stGetCacheId(2));
    }
	protected function stGetCacheId($key,$name = null)
	{
		$cache_id = parent::getCacheId($name);
		return $cache_id.'_'.$key;
	}
}