<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{stblogrecentarticles}transformer>stblogrecentarticles_60e754aaa0e1ee2265f507b23315a39f'] = 'Artículos recientes';
$_MODULE['<{stblogrecentarticles}transformer>stblogrecentarticles-column_c7d463ae029acff1301095868842750b'] = 'Artículos recientes';
$_MODULE['<{stblogrecentarticles}transformer>stblogrecentarticles-column_a7732fa3e7982a9bc842bba1d5b586f8'] = 'No hay artículos recientes';
$_MODULE['<{stblogrecentarticles}transformer>stblogrecentarticles-footer_c7d463ae029acff1301095868842750b'] = 'Artículos recientes';
$_MODULE['<{stblogrecentarticles}transformer>stblogrecentarticles-footer_a7732fa3e7982a9bc842bba1d5b586f8'] = 'No hay artículos recientes';
$_MODULE['<{stblogrecentarticles}transformer>stblogrecentarticles-home_527764dee39e255934d3e701759d5785'] = 'Artículos recientes';
$_MODULE['<{stblogrecentarticles}transformer>stblogrecentarticles-home_a7732fa3e7982a9bc842bba1d5b586f8'] = 'No hay artículos recientes';
