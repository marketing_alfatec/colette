<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{productscategory}transformer>productscategory_db33983df8ef521000b0ab60dcb5a83f'] = 'Categoría de productos';
$_MODULE['<{productscategory}transformer>productscategory_0157084bddd8b408e1cdaba00f54a009'] = 'Mostrar productos de la misma categoría en la página producto';
$_MODULE['<{productscategory}transformer>productscategory_462390017ab0938911d2d4e964c0cab7'] = 'Parámetros actualizados con éxito';
$_MODULE['<{productscategory}transformer>productscategory_f4f70727dc34561dfde1a3c529b6205c'] = 'Parámetros';
$_MODULE['<{productscategory}transformer>productscategory_b6bf131edd323320bac67303a3f4de8a'] = 'Mostrar el precio del producto';
$_MODULE['<{productscategory}transformer>productscategory_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activado';
$_MODULE['<{productscategory}transformer>productscategory_b9f5c797ebbf55adccdd8539a65a0241'] = 'Desactivado';
$_MODULE['<{productscategory}transformer>productscategory_70f9a895dc3273d34a7f6d14642708ec'] = 'Mostrar el precio del producto en el bloque';
$_MODULE['<{productscategory}transformer>productscategory_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{productscategory}transformer>productscategory_4aae87211f77aada2c87907121576cfe'] = 'otros productos de la misma categoría:';
$_MODULE['<{productscategory}transformer>productscategory_dd1f775e443ff3b9a89270713580a51b'] = 'Anterior';
$_MODULE['<{productscategory}transformer>productscategory_10ac3d04253ef7e1ddc73e6091c0cd55'] = 'Siguiente';
$_MODULE['<{productscategory}transformer>productscategory_a2aa72e389171b993f3f2bfa529e436c'] = 'otros productos de la misma categoría:';
$_MODULE['<{productscategory}transformer>productscategory_03c2e7e41ffc181a4e84080b4710e81e'] = 'Nuevo';
