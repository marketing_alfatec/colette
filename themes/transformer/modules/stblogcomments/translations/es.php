<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{stblogcomments}transformer>mycomments_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'Mi cuenta';
$_MODULE['<{stblogcomments}transformer>mycomments_259ee64057c3b7ac1423b6fed4787dfb'] = 'Subir un nuevo avatar (JPEG 80x80px):';
$_MODULE['<{stblogcomments}transformer>mycomments_91412465ea9169dfd901dd5e7c96dd99'] = 'Subir';
$_MODULE['<{stblogcomments}transformer>mycomments_18d2833f3521aae3659901e620253894'] = 'Usar predeterminado';
$_MODULE['<{stblogcomments}transformer>mycomments_6f74a521f22ce5cb87045f191d1e40ac'] = 'Mis comentarios';
$_MODULE['<{stblogcomments}transformer>mycomments_0b3db27bc15f682e92ff250ebb167d4b'] = 'Volver a su cuenta';
$_MODULE['<{stblogcomments}transformer>mycomments_8cf04a9734132302f96da8e113e80ce5'] = 'Inicio';
$_MODULE['<{stblogcomments}transformer>my-account_9f18ee985a83967e88020e0cd4c54264'] = 'Comentarios en el Blog';
$_MODULE['<{stblogcomments}transformer>stblogcomments-column_4a190b7396189db19d3156a040898e6d'] = 'Últimos comentarios';
$_MODULE['<{stblogcomments}transformer>stblogcomments-column_962491a09ce43604118343d78d4fdb2a'] = 'No hay comentarios';
$_MODULE['<{stblogcomments}transformer>stblogcomments_898a7414a92986350d30bf856123ca87'] = 'Deje un comentario';
$_MODULE['<{stblogcomments}transformer>stblogcomments_c8cf97015c98815234011807fce40a8e'] = 'Nombre (Obligatorio)';
$_MODULE['<{stblogcomments}transformer>stblogcomments_c5bea8ff54465b298102222778aae4ac'] = 'Debe';
$_MODULE['<{stblogcomments}transformer>stblogcomments_5a02a55144e98574da051358293abae7'] = 'identificarse';
$_MODULE['<{stblogcomments}transformer>stblogcomments_a511809f642577c076c1a6d09a0876f4'] = 'para escribir un comentario';
$_MODULE['<{stblogcomments}transformer>stblogcomments_91712ee2d5cbb3715edf4f09b7e5bce7'] = 'Debe esperar %1$d segundos para escribir de nuevo';
$_MODULE['<{stblogcomments}transformer>stblogcomments_a769b4030eabefcaed152f90bdbd315e'] = 'Gracias por su comentario';
$_MODULE['<{stblogcomments}transformer>stblogcomments_e94f81965bcc5291cd57cbff7d2fa066'] = 'Su comentario será publicado tras su aprobación';
$_MODULE['<{stblogcomments}transformer>stblogcomments_d3d2e617335f08df83599665eef8a418'] = 'Cerrar';
