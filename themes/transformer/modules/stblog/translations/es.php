<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{stblog}transformer>article_39bf253207282af5541757a9582dd11d'] = 'Artículos relacionados';
$_MODULE['<{stblog}transformer>blogs-list-grid_decbe415d8accca7c2c16d48a79ee934'] = 'Leer más';
$_MODULE['<{stblog}transformer>blogs-list-large_decbe415d8accca7c2c16d48a79ee934'] = 'Leer más';
$_MODULE['<{stblog}transformer>blogs-list-medium_decbe415d8accca7c2c16d48a79ee934'] = 'Leer más';
$_MODULE['<{stblog}transformer>category_3ea439e724fb5c54e748f6113eb28856'] = 'No hay blogs en esta categoría';
$_MODULE['<{stblog}transformer>category_2f4e54ec9bebe1122b5c23217e764828'] = 'Esta categoría no está disponible';
