<?php /* Smarty version Smarty-3.1.14, created on 2013-10-08 09:53:36
         compiled from "/Users/Fernando/Sites/Colette/modules/stproductcategoriesslider/views/templates/hook/stproductcategoriesslider.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1000904035253ba00cc1fe8-07214088%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f3b08e3e8eece3cfdbc99b029965b6ab6a4458ac' => 
    array (
      0 => '/Users/Fernando/Sites/Colette/modules/stproductcategoriesslider/views/templates/hook/stproductcategoriesslider.tpl',
      1 => 1379526529,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1000904035253ba00cc1fe8-07214088',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'column_slider' => 0,
    'product_categories' => 0,
    'p_c' => 0,
    'link' => 0,
    'pro_cate_easing' => 0,
    'pro_cate_slideshow' => 0,
    'pro_cate_s_speed' => 0,
    'pro_cate_a_speed' => 0,
    'pro_cate_pause_on_hover' => 0,
    'pro_cate_loop' => 0,
    'pro_cate_items' => 0,
    'is_homepage_secondary_left' => 0,
    'pro_cate_move' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5253ba00e7ffc8_35299849',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5253ba00e7ffc8_35299849')) {function content_5253ba00e7ffc8_35299849($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include '/Users/Fernando/Sites/Colette/tools/smarty/plugins/modifier.escape.php';
?>

<!-- MODULE Product categories slider -->
<?php $_smarty_tpl->_capture_stack[0][] = array("column_slider", null, null); ob_start(); ?><?php if ($_smarty_tpl->tpl_vars['column_slider']->value){?>_column<?php }?><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php if (isset($_smarty_tpl->tpl_vars['product_categories']->value)&&count($_smarty_tpl->tpl_vars['product_categories']->value)){?>
    <?php  $_smarty_tpl->tpl_vars['p_c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p_c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product_categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['p_c']->key => $_smarty_tpl->tpl_vars['p_c']->value){
$_smarty_tpl->tpl_vars['p_c']->_loop = true;
?>
        <section class="product_categories_slider_block<?php echo Smarty::$_smarty_vars['capture']['column_slider'];?>
 block products_block <?php if (!$_smarty_tpl->tpl_vars['column_slider']->value){?> section <?php }?>">
            <h4 class="title_block mar_b1">
                <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['p_c']->value['id_category'],$_smarty_tpl->tpl_vars['p_c']->value['link_rewrite']), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['p_c']->value['name'], 'htmlall', 'UTF-8');?>
"><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['p_c']->value['name'], 'htmlall', 'UTF-8');?>
</a>
            </h4>            
	        <?php if (isset($_smarty_tpl->tpl_vars['p_c']->value['products'])&&$_smarty_tpl->tpl_vars['p_c']->value['products']){?>
            <div id="product_categories-itemslider<?php echo Smarty::$_smarty_vars['capture']['column_slider'];?>
_<?php echo $_smarty_tpl->tpl_vars['p_c']->value['id_category'];?>
" class="flexslider product_categories-itemslider<?php echo Smarty::$_smarty_vars['capture']['column_slider'];?>
">
                <?php if ($_smarty_tpl->tpl_vars['column_slider']->value){?>
            	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./product-slider-list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null, array('products'=>$_smarty_tpl->tpl_vars['p_c']->value['products']), 0);?>

                <?php }else{ ?>
            	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./product-slider.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null, array('products'=>$_smarty_tpl->tpl_vars['p_c']->value['products']), 0);?>

                <?php }?>
        	</div>
            <script type="text/javascript">
            //<![CDATA[
            
            jQuery(function($) {
                $('#product_categories-itemslider<?php echo Smarty::$_smarty_vars['capture']['column_slider'];?>
_<?php echo $_smarty_tpl->tpl_vars['p_c']->value['id_category'];?>
 .sliderwrap').flexslider({
            		easing: "<?php echo $_smarty_tpl->tpl_vars['pro_cate_easing']->value;?>
",
                    useCSS: false,
            		slideshow: <?php echo $_smarty_tpl->tpl_vars['pro_cate_slideshow']->value;?>
,
                    slideshowSpeed: <?php echo $_smarty_tpl->tpl_vars['pro_cate_s_speed']->value;?>
,
            		animationSpeed: <?php echo $_smarty_tpl->tpl_vars['pro_cate_a_speed']->value;?>
,
            		pauseOnHover: <?php echo $_smarty_tpl->tpl_vars['pro_cate_pause_on_hover']->value;?>
,
                    direction: "horizontal",
                    animation: "slide",
            		animationLoop: <?php echo $_smarty_tpl->tpl_vars['pro_cate_loop']->value;?>
,
            		controlNav: false,
            		controlsContainer: "#product_categories-itemslider_<?php echo $_smarty_tpl->tpl_vars['p_c']->value['id_category'];?>
 .nav_top_right",
    		        itemWidth: 280,
                    minItems: getFlexSliderSize(<?php if ($_smarty_tpl->tpl_vars['column_slider']->value){?>1<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['pro_cate_items']->value;?>
<?php }?>,<?php if (isset($_smarty_tpl->tpl_vars['is_homepage_secondary_left']->value)&&$_smarty_tpl->tpl_vars['is_homepage_secondary_left']->value){?>true<?php }else{ ?>false<?php }?>),
                    maxItems: getFlexSliderSize(<?php if ($_smarty_tpl->tpl_vars['column_slider']->value){?>1<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['pro_cate_items']->value;?>
<?php }?>,<?php if (isset($_smarty_tpl->tpl_vars['is_homepage_secondary_left']->value)&&$_smarty_tpl->tpl_vars['is_homepage_secondary_left']->value){?>true<?php }else{ ?>false<?php }?>),
    		        move: <?php echo $_smarty_tpl->tpl_vars['pro_cate_move']->value;?>
,
                    prevText: '<i class="icon-left-open-3"></i>',
                    nextText: '<i class="icon-right-open-3"></i>',
                    productSlider:true
                });
                var product_categories_<?php echo $_smarty_tpl->tpl_vars['p_c']->value['id_category'];?>
_flexslider_rs<?php echo Smarty::$_smarty_vars['capture']['column_slider'];?>
;
                $(window).resize(function(){
                    clearTimeout(product_categories_<?php echo $_smarty_tpl->tpl_vars['p_c']->value['id_category'];?>
_flexslider_rs<?php echo Smarty::$_smarty_vars['capture']['column_slider'];?>
);
                    product_categories_<?php echo $_smarty_tpl->tpl_vars['p_c']->value['id_category'];?>
_flexslider_rs<?php echo Smarty::$_smarty_vars['capture']['column_slider'];?>
 = setTimeout(function() {
                        var flexSliderSize = getFlexSliderSize(<?php if ($_smarty_tpl->tpl_vars['column_slider']->value){?>1<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['pro_cate_items']->value;?>
<?php }?>,<?php if (isset($_smarty_tpl->tpl_vars['is_homepage_secondary_left']->value)&&$_smarty_tpl->tpl_vars['is_homepage_secondary_left']->value){?>true<?php }else{ ?>false<?php }?>);
                        var flexslide_object = $('#product_categories-itemslider<?php echo Smarty::$_smarty_vars['capture']['column_slider'];?>
_<?php echo $_smarty_tpl->tpl_vars['p_c']->value['id_category'];?>
 .sliderwrap').data('flexslider');
                        if(flexSliderSize && flexslide_object != null )
                            flexslide_object.setVars({'minItems': flexSliderSize, 'maxItems': flexSliderSize});
                	}, 100);
                });
            });
             
            //]]>
            </script>
        	<?php }else{ ?>
        		<p><?php echo smartyTranslate(array('s'=>'No products','mod'=>'stproductcategoriesslider'),$_smarty_tpl);?>
</p>
        	<?php }?>
        </section>
    <?php } ?>
<?php }?>
<!-- /MODULE Product categories slider --><?php }} ?>