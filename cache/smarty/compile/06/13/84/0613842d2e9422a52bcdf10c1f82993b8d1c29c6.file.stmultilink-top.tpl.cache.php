<?php /* Smarty version Smarty-3.1.14, created on 2013-10-08 09:53:36
         compiled from "/Users/Fernando/Sites/Colette/modules/stmultilink/views/templates/hook/stmultilink-top.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8285333705253ba004ef9b8-65761767%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0613842d2e9422a52bcdf10c1f82993b8d1c29c6' => 
    array (
      0 => '/Users/Fernando/Sites/Colette/modules/stmultilink/views/templates/hook/stmultilink-top.tpl',
      1 => 1379526528,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8285333705253ba004ef9b8-65761767',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link_groups' => 0,
    'link_group' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5253ba005a4830_81583044',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5253ba005a4830_81583044')) {function content_5253ba005a4830_81583044($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include '/Users/Fernando/Sites/Colette/tools/smarty/plugins/modifier.escape.php';
?>

<!-- Block stlinkgroups top module -->
<?php  $_smarty_tpl->tpl_vars['link_group'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['link_group']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['link_groups']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['link_group']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['link_group']->key => $_smarty_tpl->tpl_vars['link_group']->value){
$_smarty_tpl->tpl_vars['link_group']->_loop = true;
 $_smarty_tpl->tpl_vars['link_group']->index++;
 $_smarty_tpl->tpl_vars['link_group']->first = $_smarty_tpl->tpl_vars['link_group']->index === 0;
?>
    <dl class="stlinkgroups_top fr dropdown_wrap <?php if ($_smarty_tpl->tpl_vars['link_group']->first){?>first-item<?php }?> hidden-phone">
        <dt class="dropdown_tri">
        <div class="dropdown_tri_inner">
        <?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['link_group']->value['name'], 'html', 'UTF-8');?>

        <?php if (is_array($_smarty_tpl->tpl_vars['link_group']->value['links'])&&count($_smarty_tpl->tpl_vars['link_group']->value['links'])){?><b></b><?php }?>
        </div>
        </dt>
        <dd class="dropdown_list dropdown_right">
        <ul>
        <?php if ($_smarty_tpl->tpl_vars['link_group']->value['links']){?>
		<?php  $_smarty_tpl->tpl_vars['link'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['link']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['link_group']->value['links']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['link']->key => $_smarty_tpl->tpl_vars['link']->value){
$_smarty_tpl->tpl_vars['link']->_loop = true;
?>
			<li>
        		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['link']->value['title'], 'html', 'UTF-8');?>
" rel="nofollow" <?php if ($_smarty_tpl->tpl_vars['link']->value['new_window']){?> target="_blank" <?php }?>>
                    <?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['link']->value['label'], 'html', 'UTF-8');?>

        		</a>
			</li>
		<?php } ?>
		<?php }?>
		</ul>
        </dd>
    </dl>
<?php } ?>
<!-- /Block stlinkgroups top module --><?php }} ?>