<?php /* Smarty version Smarty-3.1.14, created on 2013-10-08 09:53:36
         compiled from "/Users/Fernando/Sites/Colette/modules/steasycontent/views/templates/hook/steasycontent-footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14729595005253ba00bc2fb1-48264633%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c32fee4729553c2ae555a55d0f5aa08ba51567ca' => 
    array (
      0 => '/Users/Fernando/Sites/Colette/modules/steasycontent/views/templates/hook/steasycontent-footer.tpl',
      1 => 1379526527,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14729595005253ba00bc2fb1-48264633',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'easy_content' => 0,
    'ec' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5253ba00c736b6_87615909',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5253ba00c736b6_87615909')) {function content_5253ba00c736b6_87615909($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include '/Users/Fernando/Sites/Colette/tools/smarty/plugins/modifier.escape.php';
?>
<!-- MODULE st easy content -->
<?php if (count($_smarty_tpl->tpl_vars['easy_content']->value)>0){?>
    <?php  $_smarty_tpl->tpl_vars['ec'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ec']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['easy_content']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ec']->key => $_smarty_tpl->tpl_vars['ec']->value){
$_smarty_tpl->tpl_vars['ec']->_loop = true;
?>
    <section id="easycontent_<?php echo $_smarty_tpl->tpl_vars['ec']->value['id_st_easy_content'];?>
" class="<?php if ($_smarty_tpl->tpl_vars['ec']->value['hide_on_mobile']){?>hidden-phone<?php }?> easycontent span<?php if ($_smarty_tpl->tpl_vars['ec']->value['span']){?><?php echo $_smarty_tpl->tpl_vars['ec']->value['span'];?>
<?php }else{ ?>3<?php }?> block">
        <?php if ($_smarty_tpl->tpl_vars['ec']->value['title']){?>
        <a href="javascript:;" class="opener visible-phone">&nbsp;</a>
        <h4 class="title_block">
            <?php if ($_smarty_tpl->tpl_vars['ec']->value['url']){?><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ec']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['ec']->value['title'], 'html', 'UTF-8');?>
"><?php }?>
            <?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['ec']->value['title'], 'html', 'UTF-8');?>

            <?php if ($_smarty_tpl->tpl_vars['ec']->value['url']){?></a><?php }?>
        </h4>
        <?php }?>
    	<div class="footer_block_content <?php if (!$_smarty_tpl->tpl_vars['ec']->value['title']){?>keep_open<?php }?>">
            <?php echo stripslashes($_smarty_tpl->tpl_vars['ec']->value['text']);?>

    	</div>
    </section>
    <?php } ?>
<?php }?>
<!-- MODULE st easy content --><?php }} ?>